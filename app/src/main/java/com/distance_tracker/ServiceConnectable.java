package com.distance_tracker;

public interface ServiceConnectable {

    void onServiceConnected();

    void onServiceDisconnected();
}
