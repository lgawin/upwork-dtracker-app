package com.distance_tracker;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class TrackingService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient
        .OnConnectionFailedListener, LocationListener {

    private final IBinder binder = new TrackingServiceBinder();

    private GoogleApiClient locationClient;
    private Location currentLocation;
    private float distance;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public float getCurrentDistanceInMeters() {
        return distance;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    @Override
    public void onCreate() {
        locationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        locationClient.connect();
        return START_STICKY;
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(locationClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        // nop
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // nop
    }

    @Override
    public void onLocationChanged(Location location) {
        float[] distances = new float[1];
        if (currentLocation != null && location != null) {
            Location.distanceBetween(currentLocation.getLatitude(), currentLocation.getLongitude(),
                    location.getLatitude(), location.getLongitude(),
                    distances);
            distance += distances[0];
        }
        currentLocation = location;
    }

    public class TrackingServiceBinder extends Binder {

        public TrackingService getService() {
            return TrackingService.this;
        }
    }
}
