package com.distance_tracker;

public interface PeriodicallyUpdated {

    int getPeriodicDelayMillis();

    void onPeriodicUpdate();
}
