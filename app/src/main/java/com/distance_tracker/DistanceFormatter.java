package com.distance_tracker;

public class DistanceFormatter {
    public String formatMeters(float distanceInMeters) {
        return (distanceInMeters/1000.0) + " km";
    }
}
