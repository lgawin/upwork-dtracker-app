package com.distance_tracker;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

public abstract class PeriodicallyUpdatingTrackingServiceConnectableActivity extends Activity
        implements ServiceConnectable, PeriodicallyUpdated {

    private TrackingService trackingService;
    private boolean isBound = false;
    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            TrackingService.TrackingServiceBinder binder = (TrackingService.TrackingServiceBinder) service;
            trackingService = binder.getService();
            isBound = true;
            PeriodicallyUpdatingTrackingServiceConnectableActivity.this.onServiceConnected();
        }

        public void onServiceDisconnected(ComponentName arg0) {
            isBound = false;
            PeriodicallyUpdatingTrackingServiceConnectableActivity.this.onServiceDisconnected();
        }
    };

    private Handler handler = new Handler();
    private Runnable periodicUpdateRunnable = new Runnable() {

        @Override
        public void run() {
            onPeriodicUpdate();
            handler.postDelayed(periodicUpdateRunnable, getPeriodicDelayMillis());
        }
    };

    private void schedulePeriodicUpdate() {
        handler.postDelayed(periodicUpdateRunnable, getPeriodicDelayMillis());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindService(new Intent(getContext(), TrackingService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isBound) {
            onPeriodicUpdate();
        }

        schedulePeriodicUpdate();
    }

    @Override
    protected void onPause() {
        unschedulePeriodicUpdates();
        super.onPause();
    }

    private void unschedulePeriodicUpdates() {
        handler.removeCallbacks(periodicUpdateRunnable);
    }

    @Override
    protected void onDestroy() {
        unbindService(serviceConnection);
        super.onDestroy();
    }

    private Context getContext() {
        return this;
    }

    protected TrackingService getTrackingService() {
        return trackingService;
    }

    @Override
    public void onServiceConnected() {
        onPeriodicUpdate();
    }

    @Override
    public void onServiceDisconnected() {
        unschedulePeriodicUpdates();
    }
}
