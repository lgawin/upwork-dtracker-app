package com.distance_tracker;

import android.os.Bundle;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DistanceActivity extends PeriodicallyUpdatingTrackingServiceConnectableActivity {

    private static final int DELAY_MILLIS = 10000;

    @Bind(R.id.text_distance) TextView currentDistance;
    private DistanceFormatter distanceFormatter = new DistanceFormatter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_distance);
        ButterKnife.bind(this);
    }

    @Override
    public void onPeriodicUpdate() {
        showDistance();
    }

    @Override
    public int getPeriodicDelayMillis() {
        return DELAY_MILLIS;
    }

    @Override
    public void onServiceConnected() {
        showDistance();
    }

    private void showDistance() {
        currentDistance.setText(distanceFormatter.formatMeters(getTrackingService().getCurrentDistanceInMeters()));
    }
}
