package com.distance_tracker;

import android.location.Location;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LocationActivity extends PeriodicallyUpdatingTrackingServiceConnectableActivity {

    private static final int DELAY_MILLIS = 10000;

    @Bind(R.id.text_location) TextView currentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_location);
        ButterKnife.bind(this);
    }

    @Override
    public int getPeriodicDelayMillis() {
        return DELAY_MILLIS;
    }

    @Override
    public void onPeriodicUpdate() {
        showLocation();
    }

    private void showLocation() {
        TrackingService trackingService = getTrackingService();
        currentLocation.setText(describeLocation(trackingService.getCurrentLocation()));
    }

    private String describeLocation(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        return String.format("%s%c, %s%c",
                Math.abs(latitude), Math.signum(latitude) > 0 ? 'N' : 'S',
                Math.abs(longitude), Math.signum(longitude) > 0 ? 'E' : 'W');
    }

}
